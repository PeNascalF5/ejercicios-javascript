/*

Escribe una función "calculadora" que se le pasa como parámetros 
(num, operador, num) , ejecuta la operación dada con los números introducidos
y devuelve el resultado. Si se intenta dividir por 0, debe devolver un
mensaje de error

calculadora(2, '/', 2) -> devuelve 1
calculadora(10, '-', 7) -> devuelve 3
calculadora(2, '*', 16) -> devuelve 32
calculadora(2, '-', 2) -> devuelve 0
calculadora(15, '+', 26) -> devuelve 41
calculadora(2, '+', 2) -> devuelve 4
calculadora(2, "/", 0) -> devuelve "Can't divide by 0!"
*/

// const { calculadora } = require('./calculadora.js')

test.skip('', function() {})
