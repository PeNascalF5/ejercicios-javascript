/*

Escribe una función "leerMedio" que toma una cadena de texto y devuelve los
caracteres del medio. Si es de longitud impar, devuelve el caracter central,
es de longitud par devuelve los dos caracteres centrales.

Ejemplos:

leerMedio("test") ➞ "es"
leerMedio("testing") ➞ "t"
leerMedio("middle") ➞ "dd"
leerMedio("A") ➞ "A"

*/

// const { leerMedio } = require('./leer-el-medio.js')

test.skip('', function() {})
