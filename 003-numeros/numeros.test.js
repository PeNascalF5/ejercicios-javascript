/*
Aquí no hace falta un archivo de programa. Simplemente completa los
tests para que funcionen utilizando las ayudas 
*/

test.skip('operaciones con números', function() {
  // NOTA: los números pueden ser enteros ...
  let entero1 = 7
  let entero2 = 5

  // NOTA: ... o decimales
  let decimal1 = 2.45
  let decimal2 = 1.22

  // TAREA: cambia el undefined en .toBe() para que se cumplan los tests

  expect(entero1 + entero2).toBe(undefined)
  expect(entero1 - entero2).toBe(undefined)
  expect(entero1 * entero2).toBe(undefined)
  expect(entero1 / entero2).toBe(undefine)
})

test.skip('convertir números a texto y viceversa', function() {
  let entero1 = 7
  let entero2 = 5
  let decimal1 = 2.45
  let decimal2 = 10011.22

  // TAREA: cambia el undefined en .toBe() para que se cumplan los tests

  expect(entero1.toString()).toBe(undefined)
  expect(entero2.toString()).toBe(undefined)
  expect(decimal1.toString()).toBe(undefined)
  expect(decimal2.toString()).toBe(undefined)

  let textoEntero1 = '7'
  let textoEntero2 = '5'
  let textoDecimal1 = '2.45'
  let textoDecimal2 = '10011.22'

  // TAREA: cambia funcionOculta por la función adecuada para que se cumpla el resultado

  expect(funcionOculta(textoEntero1)).toBe(7)
  expect(funcionOculta(textoEntero2)).toBe(5)
  expect(funcionOculta(textoDecimal1)).toBe(2.45)
  expect(funcionOculta(textoDecimal2)).toBe(10011.22)

  // TAREA: completar el resultado de estas funciones
  /// cambia el undefined final por el resultado esperado

  expect(parseInt(textoEntero1)).toBe(undefined)
  expect(parseInt(textoDecimal1)).toBe(undefined)
})
