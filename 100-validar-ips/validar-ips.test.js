/*

Crear la función "validarIP" en validar-ips.js cuyo argumento es una cadena
tipo IP y devuelve verdadero si la cadena es váida y falso si no

Comprobaremos:
  * Es una cadena de texto que consiste en cuatro números decimales separados por puntos
  * Los números están entre 0 y 255
  * Una IP no puede tener el último número como 0 (se usa como referencia a una red)
  * El caso especial 255.255.255.255 no es una IP válida (se usa como broadcast)
  * El caso espcial 0.0.0.0 es válido (se usa como enlace con todas las interfaces
    de un dispositivo

*/

const { validarIp } = require('./validar-ips.js')

test.skip('Válidas', function () {
  expect(validarIp('1.1.1.1')).toBe(true)
  expect(validarIp('192.168.1.1')).toBe(true)
  expect(validarIp('10.0.0.1')).toBe(true)
  expect(validarIp('127.0.0.1')).toBe(true)
  expect(validarIp('0.0.0.0')).toBe(true)
})

test.skip('No válidas', function () {
  expect(validarIp('255.255.255.255')).toBe(false)
  expect(validarIp('1.1.1.0')).toBe(false)
  expect(validarIp('10.0.1')).toBe(false)
})