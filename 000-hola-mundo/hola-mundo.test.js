/*

Modifica la función "hola" del archivo "hola-mundo.js" 
para que devuelva el texto "Hola, mundo"

*/

const { hola } = require('./hola-mundo.js')

test('Hola, mundo', function() {
  expect(hola()).toBe('Hola, mundo')
})
