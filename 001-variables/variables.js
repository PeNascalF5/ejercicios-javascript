/*
modifica las declaraciones de las variables (let xxx = ...)
para que se cumplan los test
*/

function variableTipoTexto() {
  let resultado = undefined

  return resultado
}

function variableTipoEntero() {
  let resultado = undefined

  return resultado
}

function variableTipoDecimal() {
  let resultado = undefined

  return resultado
}

function variableTipoBooleanoVerdadero() {
  let resultado = undefined

  return resultado
}

function variableTipoBooleanoFalso() {
  let resultado = undefined

  return resultado
}

function variableTipoNull() {
  let resultado = undefined

  return resultado
}

// Estas líneas son para los tests, no las borres

module.exports = {
  variableTipoTexto,
  variableTipoEntero,
  variableTipoDecimal,
  variableTipoBooleanoVerdadero,
  variableTipoBooleanoFalso,
  variableTipoNull
}
