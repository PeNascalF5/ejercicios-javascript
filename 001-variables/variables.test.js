/*
Define en cada función las variables correspondientes para que se cumplan los tests
*/

const {
  variableTipoTexto,
  variableTipoEntero,
  variableTipoDecimal,
  variableTipoBooleanoVerdadero,
  variableTipoBooleanoFalso,
  variableTipoNull
} = require('./variables.js')

test.skip('variables de tipo texto', function() {
  expect(variableTipoTexto()).toBe('esto es un texto')
})

test.skip('variables de tipo entero', function() {
  expect(variableTipoEntero()).toBe(17)
})

test.skip('variables de tipo decimal', function() {
  expect(variableTipoDecimal()).toBe(1.7)
})

test.skip('variables de tipo booleano (verdadero y falso)', function() {
  expect(variableTipoBooleanoVerdadero()).toBe(true)
  expect(variableTipoBooleanoFalso()).toBe(false)
})

test.skip('variables de tipo null', function() {
  expect(variableTipoNull()).toBe(null)
})
