/*

Hacer piedra-papel-tijera.js una función "partida"
que resuelva el ganador de una partida de piedra, 
papel y tijera.

Los argumentos son la elección de los jugadores y
el resultado es "jugador 1", "jugador 2" o "empate"
en función del resultado de la partida

Escribir los tests correspondientes y la función

*/

const { partida } = require('./piedra-papel-tijera.js')

test.skip('Jugador 1 saca piedra', function () {
  expect(partida('piedra', 'tijera')).toBe('jugador 1')
  expect(partida('piedra', 'papel')).toBe('jugador 2')
  expect(partida('piedra', 'piedra')).toBe('empate')
})

// Completa con los tests necesarios para todos casos