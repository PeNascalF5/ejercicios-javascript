/*
Aquí no hace falta un archivo de programa. Simplemente completa los
tests para que funcionen utilizando los métodos del tipo string
*/

test.skip('longitud de una cadena de texto', function () {
  let texto = 'esto es un texto largo'

  let resultado = null // cambia esta línea utilizando un método de la variable texto

  expect(resultado).toBe(22)
})

test.skip('cambiar a mayúsculas y minúscula', function () {
  let texto = 'Esto es un texto largo'

  let enMayusculas = null // cambia esta línea utilizando un método de la variable texto
  let enMinusculas = null // cambia esta línea utilizando un método de la variable texto

  expect(enMayusculas).toBe('ESTO ES UN TEXTO LARGO')
  expect(enMinusculas).toBe('esto es un texto largo')
})

test.skip('trabajar con posiciones dentro del texto', function () {
  let texto = 'Esto es un texto largo'

  let primerCaracter = null // cambia esta línea
  let cuartoCaracter = null // cambia esta línea
  let ultimoCaracter = null // cambia esta línea
  let penultimoCaracter = null // cambia esta línea

  expect(primerCaracter).toBe('E')
  expect(cuartoCaracter).toBe('o')
  expect(ultimoCaracter).toBe('o')
  expect(penultimoCaracter).toBe('g')
})

test.skip('encontrar trozos dentro del texto', function () {
  let texto = 'Esto es un texto largo'

  let posicionPrimeraS = null // cambia esta línea para encontrar la primera s
  let posicionPalabraTexto = null // cambia esta línea la palabra texto

  expect(posicionPrimeraS).toBe(1)
  expect(posicionPalabraTexto).toBe(11)
})

test.skip('sustituye trozos dentro del texto', function () {
  let texto = 'Esto es un texto largo'

  let cambiado = null // cambia esta línea

  expect(cambiado).toBe('Esto es un párrafo largo')
})

test.skip('interpolar cadenas', function () {
  /*

  Aprender sobre interpolación de texto en javascript (string interpolation)
  Diferenciar ' (comilla simple), " (comilla doble) y ` (backtick o tilde invertida)

  */

  let nombre = null // cambia esta variable
  let pueblo = null // y esta variable para cumplir el test

  let saludo = `Hola, me llamo ${nombre} y soy de ${pueblo}`

  expect(saludo).toBe('Hola, me llamo Luke Skywalker y soy de Tatooine')

  let buscaYCaptura = null // cambia esta variable con backticks para cumplir el test
  expect(buscaYCaptura).toBe(`Se busca a Luke Skywalker de Tatooine, vivo o muerto. Se ofrece recompensa`)

})