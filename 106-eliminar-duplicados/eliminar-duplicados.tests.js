/*

Escribe una función "sinDuplicados" que se le pasa como parámetros 
una lista de elementos y devuelve una lista nueva con los elementos en el
mismo orden, pero sin elemento duplicados.

* La función debe funcionar con listas de números enteros y cadenas de texto
* Se distinguen mayúsculas y minúsculas.

Ejemplos:

sinDuplicados([1, 0, 1, 0]) ➞ [1, 0]
sinDuplicados(["The", "big", "cat"]) ➞ ["The", "big", "cat"]
sinDuplicados(["John", "Taylor", "John"]) ➞ ["John", "Taylor"]
sinDuplicados([3, 'Apple', 3, 'Orange', 'Apple']) ➞ [3, 'Apple', 'Orange']

*/

// const { sinDuplicados } = require('./eliminar-duplicados.js')

test.skip('', function() {})
