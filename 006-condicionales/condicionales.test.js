/*

Referencias

https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Math#Comparison_operator
https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_JavaScript/Control/Condicionales

(se recomienda usar para las comparaciones === y !== en vez de == y != para
evitar errores)

*/

const { buenDia, diaSemana, esPar } = require('./condicionales.js')

test.skip('Resultados de las comparaciones', function() {
  // una comparación siempre equivale a true o a false

  expect(2 === 2).toBe(/* escribe aquí el resultado de la comparación */)
  expect(2 !== 2).toBe(/* escribe aquí el resultado de la comparación */)
  expect(2 >= 2).toBe(/* escribe aquí el resultado de la comparación */)
  expect(2 <= 2).toBe(/* escribe aquí el resultado de la comparación */)
  expect(1 > 2).toBe(/* escribe aquí el resultado de la comparación */)
  expect(1 < 2).toBe(/* escribe aquí el resultado de la comparación */)

  let s1 = 'abcd'
  let s2 = 'efg'
  expect(s1 > s2).toBe(/* escribe aquí el resultado de la comparación */)
  expect(
    s1.length > s2.length
  ).toBe(/* escribe aquí el resultado de la comparación */)
})

test.skip('Operadores lógicos', function() {
  // simples

  expect(true && true).toBe(/* escribe aquí el resultado de la comparación */)
  expect(false && true).toBe(/* escribe aquí el resultado de la comparación */)
  expect(true && false).toBe(/* escribe aquí el resultado de la comparación */)

  expect(true || true).toBe(/* escribe aquí el resultado de la comparación */)
  expect(false || true).toBe(/* escribe aquí el resultado de la comparación */)
  expect(true || false).toBe(/* escribe aquí el resultado de la comparación */)

  expect(!true).toBe(/* escribe aquí el resultado de la comparación */)
  expect(!false).toBe(/* escribe aquí el resultado de la comparación */)

  // pero se pueden complicar para esto están los paréntesis

  expect(
    true && false && !true
  ).toBe(/* escribe aquí el resultado de la comparación */)
  expect(
    true || !(true && (false || true))
  ).toBe(/* escribe aquí el resultado de la comparación */)
})

test.skip('if simple', function() {
  /* 
    Escribe en el archivo "condicionales.js" una función "buenDia" a la que
    se le pasa un número que representa una hora y devuelve:

      * "buenos días" si la hora está entre las 6 y las 14
      * "buenas tardes" si la hora está entre las 14 y las 20
      * "buenas noches si la hora es menor que 6 o mayor que 20
   
  */

  // completa los tests y escribe el código

  expect(buenDia(6)).toBe('buenos días')
  // expect ...
})

test.skip('día de la semana', function() {
  /*
    Escribe en el archivo "condicionales.js" una función "diaSemana" a la que
    se le pasa un número que representa el día de la semana y devuelve el
    nombre del día

    ej.: diaSemana(1) ➞ "lunes" ... diaSemana(7) ➞ "domingo"

    * BONUS 1: si lo has hecho con "if", hazlo con "switch/case"
    * BONUS 2: hazlo sin utilizar "if" ni "switch/case"

  */

  // completa los tests y escribe el código

  expect(diaSemana(1)).toBe('lunes')
  // expect ...
})

test.skip('esPar', function() {
  /*
    Previo: conozcamos el operador "%". Este operador devuelve el resto de la división entre dos números

    Ejemplos:
      4 % 2 ➞ 0
      4 % 3 ➞ 1
      5 % 2 ➞ 1
      5 % 3 ➞ 2
  */

  expect(1 % 2).toBe(/* escribe el resultado */)
  expect(2 % 1).toBe(/* escribe el resultado */)
  expect(6 % 2).toBe(/* escribe el resultado */)
  expect(6 % 3).toBe(/* escribe el resultado */)
  expect(6 % 4).toBe(/* escribe el resultado */)

  /*
    Con este nuevo conocimiento, escribir una función "esPar" que devuelve si
    el número que se le pasa como parámetor es par o no

    * BONUS: hazlo primero utilizando "if" y luego sin utilizar "if" (ni "switch")

  */

  // completa los tests y escribe el código

  expect(esPar(1)).toBe(false)
  expect(esPar(2)).toBe(true)
  // expect ...
})
