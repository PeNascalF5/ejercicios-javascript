/*

Escribe dos funciones:

una función celsiusToFarenheit que recibe un argumento en 
grados centígrados y devuelve el valor en grados Farenheit

y la función inversa farenheitToCelsius

Nota: las fórmulas de conversión son:

T(°F) = T(°C) × 9/5 + 32 

T(°C) = (T(°F) - 32) × 5/9

*/

const { celsiusToFarenheit, farenheitToCelsius } = require('./funciones.js')

test.skip('Celsius a Farenheit', function () {
  expect(celsiusToFarenheit(-10)).toBeCloseTo(14.0)
  expect(celsiusToFarenheit(0)).toBeCloseTo(32.0)
  expect(celsiusToFarenheit(1)).toBeCloseTo(33.8)
  expect(celsiusToFarenheit(2)).toBeCloseTo(35.6)
  expect(celsiusToFarenheit(3)).toBeCloseTo(37.4)
  expect(celsiusToFarenheit(10)).toBeCloseTo(50.0)
})

test.skip('Farenheit a Celsius', function () {
  expect(farenheitToCelsius(0)).toBeCloseTo(-17.78)
  expect(farenheitToCelsius(10)).toBeCloseTo(-12.22)
  expect(farenheitToCelsius(20)).toBeCloseTo(-6.67)
  expect(farenheitToCelsius(30)).toBeCloseTo(-1.11)
  expect(farenheitToCelsius(32)).toBeCloseTo(0)
  expect(farenheitToCelsius(40)).toBeCloseTo(4.44)
  expect(farenheitToCelsius(50)).toBeCloseTo(10.00)
  expect(farenheitToCelsius(60)).toBeCloseTo(15.56)
  expect(farenheitToCelsius(70)).toBeCloseTo(21.11)
  expect(farenheitToCelsius(80)).toBeCloseTo(26.67)
  expect(farenheitToCelsius(90)).toBeCloseTo(32.22)
  expect(farenheitToCelsius(98.6)).toBeCloseTo(37)
})
