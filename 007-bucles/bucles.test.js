/*

Referencias

https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Sentencias/for...of
(vale con el principio de la página)

https://lenguajejs.com/p/javascript/introduccion/bucles#inicializacin-o-incremento-mltiple

*/

const { duplicador, contarVocales, range } = require('./bucles.js')

test.skip('duplicador', function() {
  /* 
    Escribe en el archivo "bucles.js" una función "duplicador" a la que se le
    pasa una lista de números y devuelve otra lista cuyos elementos son el
    doble que la lista dada

    ej.: duplicador([1, 2, 3]) ➞ [2, 4, 6]
   
  */

  // completa los tests y escribe el código

  expect(duplicador([])).toEqual([])
  expect(duplicador([1, 2, 3])).toEqual([2, , 6])
  // expect ...
})

test.skip('contar vocales', function() {
  /*
    Escribe una función "contarVocales" que cuenta las vocales de un texto
    que se le pasa

    ej.: contarVocales('Esto es un texto') ➞ 6
    
    NOTA: para solucionarlo, ten en cuenta que el método "indexOf" que hemos
    visto en las listas es también aplicable a las cadenas de texto

    'abcde'.indexOf('b') ➞ 1
    'abcde'.indexOf('z') ➞ -1

  */

  // completa los tests y escribe el código

  expect(contarVocales('')).toBe(0)
  expect(contarVocales('Esto es un texto')).toBe(6)
  // expect ...
})

test.skip('generar series', function() {
  /*
    Escribe una función "range" que recibe un número y devuelve una lista
    desde 0 hasta ese número

    ej.: range(4) ➞ [0, 1, 2, 3, 4]

  */

  // completa los tests y escribe el código

  expect(range(0)).toEqual([0])
  expect(range(7)).toEqual([0, 1, 2, 3, 4, 5, 6, 7])
  // expect ...
})
