/*
Aquí no hace falta un archivo de programa. Simplemente completa los
tests para que funcionen utilizando las ayudas 

Para empezar puedes leer
https://www.escuelajavascript.com/matrices-o-arrays-en-javascript-que-son/
https://aprendiendo-javascript.readthedocs.io/es/latest/js_estructuras_de_datos.html#arrays-metodos

*/

test.skip('Declarar un array y acceder a los elementos de un array', function () {
  // Declara un array de tal forma que cumpla los tests siguientes
  let miArray = []

  expect(miArray[3]).toBe(null)
  expect(miArray[1]).toBe('hola')
  expect(miArray[2]).toBe(22.22)
  expect(miArray[0]).toBe(12)
  expect(miArray[4]).toBe(false)

  // Utilizando métodos y propiedades del array, haz que se cumplan los test

  let longitud = null // cambiar por miArray...
  expect(longitud).toBe(5)

})

test.skip('Modificar un array por los extremos', function () {
  let miArray = ['uno', 2]

  // introduce una línea para añadir un elemento al final del array
  // miArray....
  expect(miArray).toEqual(['uno', 2, 'esto va al final'])

  // introduce una línea para añadir un elemento al principio del array
  // miArray....
  expect(miArray).toEqual(['esto va al principio', 'uno', 2, 'esto va al final'])

  // introduce una línea para sacar el primer elemento de un array
  let primerElemento = null // miArray...
  expect(primerElemento).toBe('esto va al principio')

  // date cuenta de que el array también cambia
  expect(miArray).toEqual(['uno', 2, 'esto va al final'])

  // introduce una línea para sacar el último elemento de un array
  let ultimoElemento = null // miArray...
  expect(ultimoElemento).toBe('esto va al final')

  // date cuenta de que el array también cambia
  expect(miArray).toEqual([/* mete aquí los valores actuales */])

})

test.skip('Buscar elementos en un array', function () {
  let miArray = ['a', 'b', 'c', 'd', 'e', 'a', 'b', 'c', 'd', 'e']

  // Busca el índice donde se encuentran las siguientes letras por primera vez

  let primeraA = null // miArray....
  expect(primeraA).toBe(0)

  let primeraC = null // miArray....
  expect(primeraC).toBe(2)

  // Busca el índice donde se encuentran las siguientes letras por última vez

  let ultimaA = null // miArray....
  expect(ultimaA).toBe(5)

  let ultimaC = null // miArray....
  expect(ultimaC).toBe(7)

  // Cambia null por el valor que se devuelve si no se encuentra el elemento

  expect(miArray.indexOf('z')).toBe(/* escribe la salida esperada */)

})

test.skip('Juntar arrays', function () {
  let miPrimerArray = [1, 2]
  let miSegundoArray = ['uno', 'dos']

  // escribe una línea que junte los dos arrays
  let miArrayTotal = null // junta los arrays

  expect(miArrayTotal).toEqual([1, 2, 'uno', 'dos'])
  expect(miPrimerArray).toEqual([1, 2])
  expect(miSegundoArray).toEqual(['uno', 'dos'])

})


test.skip('Obtener trozos de arrays', function () {
  let miArray = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

  // escribe una línea que junte los dos arrays
  let miSubArray = null // miArray....

  expect(miSubArray).toEqual(['c', 'd', 'e', 'f'])

})


