# EJERCICIOS JAVASCRIPT

## Requisitos

- Instalar `nvm` y `node`: https://github.com/nvm-sh/nvm
- Instalar `jest`: `npm install -g jest`

## Funcionamiento

Todos los ejercicios están separados en un directorio. Cada uno de ellos tiene un arhcivo de código y uno de tests.

Para ejecutar todos los tests de todos los ejercicios, ejecutar

`jest`

en el directorio raíz de los ejercicios.

Para ejecutar solo los tests de un ejercicio, ejecutar

`jest <nombre del directorio del ejercicio>`

en el directorio raíz de los ejercicios.

Para ir construyendo el código poco a poco, los tests suelen estar marcados para que no se ejecuten como `test.skip`. Si queremos ejecutar un test, hay que eliminar el modificador `.skip`
